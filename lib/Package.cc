#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include "Package.hh"

namespace Parcel {

Package::Package(std::istream& from_pkginfo) : _done_deps(false), _done_ii(false), _done_prov(false), _done_replace(false)
{
    // We can't blindly trust this input - we could be called from something
    // other than the Repository ctor.
    char nextline[8192];
    while(from_pkginfo.getline(nextline, 8192)) {
        string raw_value;

        if(nextline[0] == '\0') {
            // Skip blank lines
            continue;
        }
        if(nextline[1] == '\0' || nextline[1] != ':') {
            std::cerr << "Package: invalid descriptor: " << nextline << std::endl;
        }

        raw_value = string(nextline + 2);

        switch(nextline[0]) {
        case 'P':
            // Package name
            _name = raw_value;
            break;
        case 'V':
            _version = raw_value;
            break;
        case 'T':
            _description = raw_value;
            break;
        case 'U':
            _url = raw_value;
            break;
        case 'I':
            _install_size = std::stoul(raw_value);
            break;
        case 'L':
            _license = raw_value;
            break;
        case 'A':
            _arch = raw_value;
            break;
        case 'D':
            _raw_deps = raw_value;
            break;
        case 'i':
            _raw_ii = raw_value;
            break;
        case 'p':
            _raw_prov = raw_value;
            break;
        case 'r':
            _raw_replace = raw_value;
            break;
        case 'm':
            _maintainer = raw_value;
            break;
        case 't':
            _built_at = std::stoul(raw_value);
            break;
        case 'c':
            _commit_id = raw_value;
            break;
        case 'o':
            _origin = raw_value;
            break;
        }
    }

    if(from_pkginfo.fail() && !from_pkginfo.eof()) {
        std::cerr << "I/O FAILURE parsing package information" << std::endl;
    }
}

void Package::maybe_load_depends() {
    if(!this->_done_deps) {
        this->_done_deps = true;
        std::istringstream atom_stream(_raw_deps);
        this->_dependencies = new std::set<string>({std::istream_iterator<string>(atom_stream), std::istream_iterator<string>()});
    }
}

std::set<string>* Package::depends() {
    this->maybe_load_depends();
    return this->_dependencies;
}

void Package::maybe_load_install_if() {
    if(!this->_done_ii) {
        this->_done_ii = true;
        std::istringstream atom_stream(_raw_ii);
        this->_install_if = new std::set<string>({std::istream_iterator<string>(atom_stream), std::istream_iterator<string>()});
    }
}

std::set<string>* Package::install_if() {
    this->maybe_load_install_if();
    return this->_install_if;
}

void Package::maybe_load_provides() {
    if(!this->_done_prov) {
        this->_done_prov = true;
        std::istringstream atom_stream(_raw_prov);
        this->_provides = new std::set<string>({std::istream_iterator<string>(atom_stream), std::istream_iterator<string>()});
        std::transform(this->_provides->cbegin(), this->_provides->cend(), std::inserter(this->_named_provides, this->_named_provides.end()), [](const string& elem) {
            string::size_type equals = elem.find("=");
            if(equals != string::npos) return string(elem.substr(0, equals));
            return string(elem);
        });
    }
}

std::set<string>* Package::provides() {
    this->maybe_load_provides();
    return this->_provides;
}

bool Package::can_provide(const string& name) {
    if(this->_name == name) return true;

    this->maybe_load_provides();
    for (auto const &elem : this->_named_provides) { if(elem == name) return true; }
    return false;
}

bool Package::can_provide(const string& name, const PackageVersion& version) {
    if(this->_name == name) return true;

    this->maybe_load_provides();
    for (auto const &elem : *this->_provides) {
        string::size_type equals;
        equals = elem.find("=");
        // Without a specified provider version, this provides any version
        if(equals == string::npos && elem == name) return true;
        if(elem.substr(0, equals) == name && PackageVersion(elem.substr(equals)) == version)
            return true;
    }
    return false;
}

bool Package::can_provide(const string& name, const string& version) {
    return this->can_provide(name, PackageVersion(version));
}

void Package::maybe_load_replaces() {
    if(!this->_done_replace) {
        this->_done_replace = true;
        std::istringstream atom_stream(_raw_replace);
        this->_replaces = new std::set<string>({std::istream_iterator<string>(atom_stream), std::istream_iterator<string>()});
    }
}

std::set<string>* Package::replaces() {
    this->maybe_load_replaces();
    return this->_replaces;
}

Package::~Package() {
    if(this->_done_deps) delete this->_dependencies;
    if(this->_done_ii) delete this->_install_if;
    if(this->_done_prov) delete this->_provides;
    if(this->_done_replace) delete this->_replaces;
}

}
