#include "Repository.hh"
#include <algorithm>
#include <iostream>
#include <sstream>

namespace Parcel {

using std::vector;
using std::find_if;

Repository::Repository(std::istream& from_index) {
    string nextpkg;
    // libreoffice-common p: line is current record with 4241 bytes
    char nextline[8192];

    while(from_index.getline(nextline, 8192)) {
        if(nextline[0] == '\0') {
            // Spurious empty line at beginning / end of index?
            if(nextpkg.length() == 0) { continue; }

            // Empty line means this package is completely read.
            std::istringstream stream(nextpkg);
            _packages.push_back(new Package(stream));
            nextpkg = "";
            continue;
        }
        nextpkg.append(nextline);
        nextpkg.append("\n");
    }

    if(from_index.fail() && !from_index.eof()) {
        std::cerr << "I/O FAILURE parsing repository index file" << std::endl;
    }

    for(auto pkg : _packages) _origins.insert(pkg->origin());
}

Repository::~Repository() {
    for(auto pkg : _packages) { delete pkg; }
}

bool Repository::has_package(const string& name) {
    return std::find_if(this->_packages.cbegin(), this->_packages.cend(),
                 [name](Package* elem) {
        return elem->pkgname() == name;
    }) != this->_packages.cend();
}

Package* Repository::package(const string& name) {
    vector<Package*> maybe_pkgs;

    std::copy_if(this->_packages.cbegin(), this->_packages.cend(), std::back_inserter(maybe_pkgs),
        [name](Package* elem) {
            return elem->pkgname() == name;
    });

    if(maybe_pkgs.size() == 0) return nullptr;

    return *std::max_element(maybe_pkgs.cbegin(), maybe_pkgs.cend(), [](Package* elem1, Package* elem2) {
        return elem1->pkgver() < elem2->pkgver();
    });
}

vector<Package*> Repository::providers(const string& name) {
    vector<Package*> provider_list;

    std::copy_if(this->_packages.cbegin(), this->_packages.cend(), std::back_inserter(provider_list),
                 [name](Package* elem) {
        // no version specified, so find any version
        return elem->can_provide(name);
    });

    return provider_list;
}

}
