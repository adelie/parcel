#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include "../Repository.hh"

using namespace Parcel;

int usage() {
    printf("repoquery pkgname [repofile]\n");
    return EXIT_FAILURE;
}

Repository *make_repo_from_file(string filename) {
    std::ifstream my_file(filename);
    if(!my_file) return nullptr;

    return new Repository(my_file);
}

int main(int argc, char* argv[])
{
    Repository* repo;
    Package *pkg;

    if(argc < 2) return usage();
    if(argc == 2) repo = make_repo_from_file("ActualSystemRepo");
    else repo = make_repo_from_file(string(argv[2]));

    if(!repo) {
        std::cout << "Error opening repository" << std::endl;
        return EXIT_FAILURE;
    }

    if((pkg = repo->package(string(argv[1]))) == nullptr) {
        std::cout << "No package \"" << argv[1] << "\" found." << std::endl;
        delete repo;
        return EXIT_FAILURE;
    }

    std::cout << "Details for \"" << pkg->pkgname() << "\" (version " << pkg->pkgver().version() << "):" << std::endl;

    std::cout << "\tDescription: " << pkg->pkgdesc() << std::endl;
    std::cout << "\tArchitecture: " << pkg->arch() << std::endl;
    std::cout << "\tLicense: " << pkg->license() << std::endl;
    std::cout << "\tWeb Site: " << pkg->url() << std::endl;
    std::cout << "\tMaintained by: " << pkg->maintainer() << std::endl;
    std::cout << "\tSize when installed: " << pkg->size() << " bytes" << std::endl;
    std::cout << "\tDepends on:" << std::endl;
    for(auto dep : *pkg->depends()) {
        std::cout << "\t\t" << dep << " ( ";
        for(auto provider : repo->providers(dep)) std::cout << provider->pkgname() << " ";
        std::cout << ")" << std::endl;
    }

    delete repo;

    return EXIT_SUCCESS;
}
