#include <cassert>
#include "../Package.hh"

using namespace Parcel;

void test_packageversions()
{
    PackageVersion valid("1.1-r1");
    assert(valid.version() == "1.1");
    assert(valid.revision() == 1);

    PackageVersion norev("1.2.3");
    assert(norev.version() == "1.2.3");
    assert(norev.revision() == 0);

    PackageVersion blank("");
    assert(blank.version() == "");
    assert(blank.revision() == 0);

    PackageVersion corruptrev("1.1_alpha4-rX");
    assert(corruptrev.version() == "1.1_alpha4");
    assert(corruptrev.revision() == 0);

    PackageVersion multirev("1.1-r1-r4");
    assert(multirev.version() == "1.1-r1");
    assert(multirev.revision() == 4);
}

void test_equality() {
    PackageVersion onepoint0("1.0");
    PackageVersion onepoint0r0("1.0-r0");

    assert(onepoint0 == onepoint0r0);

    PackageVersion onepoint0r1("1.0-r1");
    PackageVersion onepoint1r0("1.1-r0");

    assert(onepoint0 != onepoint0r1);
    assert(onepoint0r0 != onepoint0r1);
    assert(onepoint0r0 != onepoint1r0);
}

void test_ordering() {
    // Taken from APKBUILD.5 manpage
    PackageVersion first("1.0-r0");
    PackageVersion second("1.1_alpha2-r1");
    PackageVersion third("1.1.3_pre-r2");
    PackageVersion fourth("1.1.3-r0");
    PackageVersion fifth("1.1.3_hg-r0");
    PackageVersion sixth("1.2-r0");
    PackageVersion seventh("1.2a-r0");
    PackageVersion eighth("1.2b-r0");
    PackageVersion ninth("1.2c_alpha2-r0");
    PackageVersion tenth("1.3-r0");

    assert(first < second);
    assert(second < third);
    assert(third < fourth);
    assert(fourth < fifth);
    assert(fifth < sixth);
    assert(sixth < seventh);
    assert(seventh < eighth);
    assert(eighth < ninth);
    assert(ninth < tenth);

    /* since other ops are based on <, just ensure basics work as sanity test */
    assert(eighth <= ninth);
    assert(ninth <= ninth);

    assert(tenth > ninth);
    assert(ninth >= eighth);
    assert(eighth > seventh);
}

int main(void)
{
    test_packageversions();
    test_equality();
    test_ordering();
    return 0;
}
