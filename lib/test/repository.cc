#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include "../Repository.hh"

using namespace Parcel;

Repository *make_repo_from_file(string filename) {
    std::ifstream my_file(filename);
    if(!my_file) return nullptr;

    return new Repository(my_file);
}

std::multimap<unsigned long, string> repofiles = {
    // Number of packages in repo, Repo file name
    { 43, "NormalRepo" },
    { 43, "ExtraNewlineAtBegin" },
    { 43, "ExtraNewlineAtEnd" },
    { 43, "ExtraNewlineAtBoth" },
    { 43, "ExtraNewlineInMiddle" },
    { 1524, "ActualSystemRepo" },
    { 7320, "ActualUserRepo" }
};

void test_repository() {
    Repository *test_repo;

    for(auto repo : repofiles) {
        test_repo = make_repo_from_file(repo.second);
        if(test_repo == nullptr) {
            std::cerr << "Ack!  Can't load index file: " << repo.second << std::endl;
            continue;
        }
        assert(test_repo->package_count() == repo.first);
        delete test_repo;
    }
}

int main(void)
{
    test_repository();
    return 0;
}
