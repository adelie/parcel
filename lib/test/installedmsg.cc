#include <fstream>
#include <iostream>
#include "../Repository.hh"

using namespace Parcel;

Repository *make_repo_from_file(string filename) {
    std::ifstream my_file(filename);
    if(!my_file) return nullptr;

    return new Repository(my_file);
}

int main(void) {
    uint64_t size = 0;
    Repository *test_repo = make_repo_from_file("/lib/apk/db/installed");

    if(test_repo == nullptr) {
        std::cerr << "Ack!  Can't load installed packages." << std::endl;
        return EXIT_FAILURE;
    }

    for(auto &pkg : test_repo->packages()) {
        size += pkg->size();
    }

    size /= 1048576;

    std::cout << "OK: " << size << " MiB in " << test_repo->package_count() << " packages" << std::endl;
    delete test_repo;
    return 0;
}
