#include <cassert>
#include <cstdint>
#include <iostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>

using std::string;

namespace Parcel {

class PackageVersion {
public:
    PackageVersion(const string& from) {
        string::size_type revplace = from.rfind("-r");
        if(revplace == string::npos) {
            _version = string(from);
            _revision = 0;
        } else {
            _version = string(from, 0, revplace);
            try {
                _revision = std::stoul(from.substr(revplace + 2));
            } catch (std::invalid_argument) {
                std::cerr << "invalid revision number: " << from.substr(revplace + 2) << std::endl;
                _revision = 0;
            } catch (std::out_of_range) {
                std::cerr << "revision number unrepresentable: " << from.substr(revplace + 2) << std::endl;
                _revision = 0;
            }
        }
    }

    const string version() const {
        return this->_version;
    }

    unsigned long revision() const {
        return this->_revision;
    }

    friend inline bool operator< (const PackageVersion &left, const PackageVersion &right) {
        assert(false); // NYI
        return false;
    }
    friend inline bool operator> (const PackageVersion &left, const PackageVersion &right) { return right < left; }
    friend inline bool operator<=(const PackageVersion &left, const PackageVersion &right) { return !(left > right); }
    friend inline bool operator>=(const PackageVersion &left, const PackageVersion &right) { return !(left < right); }

    bool compare(const PackageVersion &other, bool fuzzy = false) const {
        if(!fuzzy) return (this->_version == other._version && this->_revision == other._revision);
        assert(false); // NYI
        return false;
    }

    friend inline bool operator==(const PackageVersion &left, const PackageVersion& right) {
        return left.compare(right);
    }
    friend inline bool operator!=(const PackageVersion& left, const PackageVersion& right) {
        return !(left == right);
    }

    bool operator==(const PackageVersion& other) { return this->compare(other); }
private:
    string _version;
    unsigned long _revision;
};

class Package {
public:
    const string pkgname() { return _name; }
    const PackageVersion pkgver() { return PackageVersion(_version); }
    const string pkgdesc() { return _description; }
    const string url() { return _url; }
    uint64_t size() { return _install_size; }
    const string license() { return _license; }
    const string arch() { return _arch; }
    std::set<string>* depends();
    std::set<string>* install_if();
    bool can_provide(const string& name);
    bool can_provide(const string& name, const string& version);
    bool can_provide(const string& name, const PackageVersion& version);
    std::set<string>* provides();
    std::set<string>* replaces();
    const string maintainer() { return _maintainer; }
    uint64_t builddate() { return _built_at; }
    const string commit() { return _commit_id; }
    const string origin() { return _origin; }
    ~Package();
private:
    string _name;
    string _version;
    string _description;
    string _url;
    uint64_t _install_size;
    string _license;
    string _arch;
    string _raw_deps;
    bool _done_deps;
    std::set<string>* _dependencies;
    string _raw_ii;
    bool _done_ii;
    std::set<string>* _install_if;
    string _raw_prov;
    bool _done_prov;
    std::set<string>* _provides;
    std::set<string> _named_provides;
    string _raw_replace;
    bool _done_replace;
    std::set<string>* _replaces;
    uint64_t _replaces_priority;
    string _maintainer;
    uint64_t _built_at;
    string _commit_id;
    string _origin;

    void maybe_load_depends();
    void maybe_load_install_if();
    void maybe_load_provides();
    void maybe_load_replaces();
    Package(std::istream& from_pkginfo);
    friend class Repository;
};

}
