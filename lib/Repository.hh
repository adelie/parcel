#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>
#include "Package.hh"

using std::string;

namespace Parcel {

class Repository
{
public:
    Repository(std::istream& from_index);
    ~Repository();

    /*! All packages in this repository */
    std::vector<Package*> packages() { return _packages; }
    /*! True if at least one Package is present in this repository with
     *  the specified name */
    bool has_package(const string& name);
    /*! The newest version of the package specified by `name` */
    Package* package(const string& name);
    /*! Number of packages in this repository */
    unsigned long package_count() { return _packages.size(); }
    /*! Names of every origin package in this repository */
    std::vector<string> origins() { return std::vector<string>(_origins.begin(), _origins.end()); }
    /*! Number of origin packages in this repository */
    unsigned long origin_count() { return _origins.size(); }
    /*! All packages that provide `name`. */
    std::vector<Package*> providers(const string& name);
private:
    std::vector<Package*> _packages;
    std::unordered_set<string> _origins;
};

}
