===================
 README for Parcel
===================
:Authors:
  * **A. Wilcox**, maintainer, principal developer
:Status:
  Early Alpha
:Copyright:
  © 2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains code and documentation for Parcel, the next-generation
package lifecycle management system for Adélie Linux.

The main code lives in the lib directory, which is where the Parcel library
code lives.  The utilities, tools, and Web site that comprise the Parcel system
are all based upon this library.


License
```````

As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````

Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Building
========

Use CMake.  XX: write more here
