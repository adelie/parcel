===============================
 Contribution Guide for Parcel
===============================
:Author:
  * **A. Wilcox**, documentation writer
:Status:
  Draft
:Copyright:
  © 2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains code and documentation for Parcel, the next-generation
package lifecycle management system for Adélie Linux.


License
```````
As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Testing
=======

You **must** test any changes on Adélie Linux.  It is recommended that you
additionally test your changes on at least a glibc-based Linux distribution.




Contributing Changes
====================

This section describes the usual flows of contribution to this repository.


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the `Parcel mailing list`_.

.. _Parcel mailing list: https://lists.adelielinux.org/postorius/lists/parcel.lists.adelielinux.org/
